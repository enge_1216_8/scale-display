import type { NextApiRequest, NextApiResponse } from "next";
import { prisma } from "../../util/prisma";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    try {
        if (req.method !== "POST") {
            res.status(400).end();
        }
        let body = req.body;
        if (req.headers["content-type"] === "application/json") {
            if (typeof body === "string") {
                body = JSON.parse(body);
            }
        }
        const { weight } = body;
        console.log(weight);
        await prisma.weightMeasurement.create({
            data: {
                weight,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        });

        res.status(200).end();
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: err });
    }
}
