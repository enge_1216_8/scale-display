import type { NextApiRequest, NextApiResponse } from "next";
import { prisma } from "../../util/prisma";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    try {
        if (req.method !== "GET") {
            res.status(400).end();
        }
        const { userID } = req.query;

        const userIDNumber = parseInt(userID as string);
        console.log(userIDNumber);

        if (userID && isNaN(userIDNumber)) {
            throw new Error("Improper User ID");
        }

        console.log(userIDNumber);

        const weights = await prisma.weightMeasurement.findMany({
            where: {
                linkedUserId: userIDNumber || undefined,
            },
        });

        res.status(200).json({ weights: weights });
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: err });
    }
}
