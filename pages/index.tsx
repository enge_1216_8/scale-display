import {
    Container,
    Fab,
    Grid,
    Paper,
    Tabs,
    Tab,
    TextField,
    Typography,
    AppBar,
} from "@mui/material";
import type { NextPage } from "next";
import { ChangeEvent, useState } from "react";
import { TabContext, TabPanel } from "@mui/lab";

import styles from "../styles/Home.module.css";
import { TabLabel } from "../util/TabLabel";
import ScaleInfo from "../components/ScaleInfo";
import WeightChart from "../components/WeightChart";

const Home: NextPage = () => {
    const [tab, setTab] = useState<string>(TabLabel.ScaleInfo);
    const handleTabChange = (event: ChangeEvent<{}>, newValue: string) => {
        setTab(newValue);
    };
    return (
        <div className={styles.HomePage}>
            <TabContext value={tab}>
                <AppBar position="relative">
                    <Tabs
                        value={tab}
                        onChange={handleTabChange}
                        variant="fullWidth"
                        centered
                    >
                        <Tab
                            label="Scale Information"
                            value={TabLabel.ScaleInfo}
                        />
                        <Tab label="User View" value={TabLabel.UserView} />
                    </Tabs>
                </AppBar>
                <Container maxWidth="lg" className={styles.MainContainer}>
                    <TabPanel
                        className={styles.MainTabPanel}
                        value={TabLabel.ScaleInfo}
                    >
                        <div className={styles.MainGrid}>
                            <WeightChart />
                            <ScaleInfo />
                        </div>
                    </TabPanel>
                    <TabPanel
                        className={styles.MainTabPanel}
                        value={TabLabel.UserView}
                    ></TabPanel>
                </Container>
            </TabContext>
        </div>
    );
};

export default Home;
