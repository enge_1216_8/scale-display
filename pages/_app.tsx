import "../styles/globals.css";
import type { AppProps } from "next/app";
import TopBar from "../components/TopBar";
import {
    createTheme,
    CssBaseline,
    Paper,
    ThemeOptions,
    ThemeProvider,
} from "@mui/material";
import { amber, grey } from "@mui/material/colors";

function MyApp({ Component, pageProps }: AppProps) {
    const themeOptions: ThemeOptions = {
        palette: {
            mode: "dark",
        },
    };
    const theme = createTheme(themeOptions);
    return (
        <div className="AppContainer">
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <TopBar />

                <Component {...pageProps} />
            </ThemeProvider>
        </div>
    );
}

export default MyApp;
