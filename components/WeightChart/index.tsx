import { Paper, useTheme } from "@mui/material";
import React from "react";
import styles from "./index.module.css";

import useSWR from "swr";
import { fetcher } from "../../util/swr";

import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
    AreaChart,
    Area,
    Label,
} from "recharts";
import { patchWeightContainingData } from "../../util/weights";

interface ScaleInfoProps {
    userID?: number;
}

const rows = [{ id: 1, weight: 30 }];

const WeightChart: React.FC<ScaleInfoProps> = (props) => {
    const { userID } = props;
    const theme = useTheme();

    const BASEURL = "/api/get-weights";
    let url = BASEURL;
    if (userID) {
        url += `?useID=${userID}`;
    }

    const { data, error } = useSWR(url, fetcher);

    if (error) return <div>failed to load</div>;
    if (!data) return <div>loading...</div>;
    const patchedData = patchWeightContainingData(data.weights);
    console.log(patchedData);

    return (
        <div className={styles.WeightChart}>
            <Paper className={styles.WeightChartPaper}>
                <ResponsiveContainer width="90%" height="90%">
                    <AreaChart
                        data={patchedData.map((data) => {
                            return {
                                ...data,
                                "Weight in G":
                                    Math.round(
                                        (data.weight + Number.EPSILON) * 100
                                    ) / 100,
                                "Weight This Load":
                                    Math.round(
                                        (data.unitWeight + Number.EPSILON) * 100
                                    ) / 100,
                            };
                        })}
                        margin={{
                            top: 10,
                            right: 30,
                            left: 0,
                            bottom: 0,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="id" />
                        <YAxis />
                        <Tooltip />
                        <Area
                            type="monotone"
                            dataKey="Weight in G"
                            stroke={theme.palette.secondary.main}
                            fill={theme.palette.secondary.main}
                        />
                        <Area
                            type="monotone"
                            dataKey="Weight This Load"
                            stroke={theme.palette.primary.main}
                            fill={theme.palette.primary.main}
                        />
                    </AreaChart>
                </ResponsiveContainer>
            </Paper>
        </div>
    );
};

export default WeightChart;
