import { AppBar, Container, Toolbar, Typography } from "@mui/material";
import React from "react";
import styles from "./index.module.css";

const TopBar: React.FC = () => {
    return (
        <AppBar position="static">
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Typography
                        className={styles.title}
                        variant="h6"
                        component="div"
                    >
                        Scale Display Demo
                    </Typography>
                    <Typography variant="subtitle1">❤️ Group 8</Typography>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default TopBar;
