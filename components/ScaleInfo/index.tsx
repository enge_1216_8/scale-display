import { Paper } from "@mui/material";
import React from "react";
import styles from "./index.module.css";

import {
    DataGrid,
    GridColDef,
    GridValueFormatterParams,
    GridValueGetterParams,
} from "@mui/x-data-grid";
import useSWR from "swr";
import { fetcher } from "../../util/swr";

interface ScaleInfoProps {
    userID?: number;
}

const columns: GridColDef[] = [
    { field: "id", headerName: "ID", width: 90 },
    {
        field: "weight",
        headerName: "Weight",
        valueFormatter: (params: GridValueFormatterParams) => {
            return `${params.value} g`;
        },
    },
    {
        field: "updatedAt",
        headerName: "Date Recorded",
        width: 200,
        valueFormatter: (params: GridValueFormatterParams<string>) => {
            if (params.value == null) {
                return "";
            }

            const date = new Date(params.value);
            return date.toLocaleDateString();
        },
    },
    {
        field: "time",
        headerName: "Time Recorded",
        width: 200,
        valueGetter: (params: GridValueGetterParams<string>) => {
            const date = new Date(params.row.updatedAt);
            return date.toLocaleTimeString();
        },
    },
];

const rows = [{ id: 1, weight: 30 }];

const ScaleInfo: React.FC<ScaleInfoProps> = (props) => {
    const { userID } = props;

    const BASEURL = "/api/get-weights";
    let url = BASEURL;
    if (userID) {
        url += `?useID=${userID}`;
    }

    const { data, error } = useSWR(url, fetcher);

    if (error) return <div>failed to load</div>;
    if (!data) return <div>loading...</div>;
    console.log(data);
    return (
        <div className={styles.ScaleInfo}>
            <Paper className={styles.ScaleInfoPaper}>
                <DataGrid rows={data.weights} columns={columns} pageSize={5} />
            </Paper>
        </div>
    );
};

export default ScaleInfo;
