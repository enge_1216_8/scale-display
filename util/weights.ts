export interface WeightContainingData {
    [key: string]: any;
    weight: number;
}

export const getUnitWeights = (weights: number[]): number[] => {
    const res = [0];
    for (let i = 1; i < weights.length; i++) {
        res[i] = Math.max(weights[i] - weights[i - 1], 0);
    }
    return res;
};

export const patchWeightContainingData = (
    weightContainingData: WeightContainingData[]
): WeightContainingData[] => {
    const weights = weightContainingData.map((data) => data.weight);
    const unitWeights = getUnitWeights(weights);
    return weightContainingData.map((data, i) => {
        const newData = { ...data, unitWeight: unitWeights[i] };
        return newData;
    });
};
